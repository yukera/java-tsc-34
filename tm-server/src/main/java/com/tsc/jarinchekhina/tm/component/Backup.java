package com.tsc.jarinchekhina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void start() {
        @NotNull final Long delay = bootstrap.getPropertyService().getBackupInterval();
        executorService.scheduleWithFixedDelay(this, 0, delay, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    public void run() {
        bootstrap.getDataService().saveBackup();
    }

    @SneakyThrows
    public void load() {
        bootstrap.getDataService().loadBackup();
    }

}
