package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserEndpoint extends IEndpoint<User> {

    @NotNull
    User createUser(@Nullable String login, @Nullable String password);

    @NotNull
    User createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User createUserWithRole(
            @Nullable Session session,
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @NotNull
    User setPassword(@Nullable Session session, @Nullable String password);

    @NotNull
    User updateUser(
            @Nullable Session session,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
