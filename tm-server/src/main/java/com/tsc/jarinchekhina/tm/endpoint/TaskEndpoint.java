package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.ITaskEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.entity.Session;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Task createTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task createTaskWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findById(session.getUserId(), id);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findByName(session.getUserId(), name);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeById(session.getUserId(), id);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
        return task.orElse(null);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().removeByName(session.getUserId(), name);
        return task.orElse(null);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().startTaskById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().startTaskByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task startTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().startTaskByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().finishTaskById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().finishTaskByIndex(session.getUserId(), index);
    }

    @NotNull
    @Override
    @WebMethod
    public Task finishTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().finishTaskByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().changeTaskStatusById(session.getUserId(), id, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().changeTaskStatusByIndex(session.getUserId(), index, status);
    }

    @NotNull
    @Override
    @WebMethod
    public Task changeTaskStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getTaskService().changeTaskStatusByName(session.getUserId(), name, status);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> findAllTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task bindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectTaskService().bindTaskByProjectId(session.getUserId(), projectId, taskId);
    }

    @NotNull
    @Override
    @WebMethod
    public Task unbindTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().checkRoles(session.getUserId(), Role.USER);
        return serviceLocator.getProjectTaskService().unbindTaskByProjectId(session.getUserId(), taskId);
    }

}
