package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.endpoint.Project;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectsListNotFoundException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "show project list";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        @Nullable List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjects(serviceLocator.getSession());
        if (projects == null) throw new ProjectsListNotFoundException();
        int index = 1;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectStatus = project.getStatus().value();
            System.out.println(index + ". " + project.getId() + ' ' + project.getName() + " (" + projectStatus + ")");
            index++;
        }
    }

}
