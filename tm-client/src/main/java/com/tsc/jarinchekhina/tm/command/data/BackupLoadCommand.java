package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class BackupLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "backup-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load backup from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BACKUP]");
        serviceLocator.getAdminEndpoint().loadBackup(serviceLocator.getSession());
    }

}
