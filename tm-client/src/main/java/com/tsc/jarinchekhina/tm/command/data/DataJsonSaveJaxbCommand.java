package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataJsonSaveJaxbCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-jaxb-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save JSON data to file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SAVE JSON DATA]");
        serviceLocator.getAdminEndpoint().saveJaxbData(serviceLocator.getSession(), true);
    }

}
