package com.tsc.jarinchekhina.tm.command.data;

import com.tsc.jarinchekhina.tm.command.AbstractDataCommand;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-binary-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load binary data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BINARY DATA]");
        serviceLocator.getAdminEndpoint().loadBinaryData(serviceLocator.getSession());
    }

}
